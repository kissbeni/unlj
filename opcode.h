
#ifndef UNLJ_OPCODE_H
#define UNLJ_OPCODE_H

#include <cstdint>

// instruction set of luajit v2.0
enum LuaJITOpcode : uint8_t
{
    /* Comparison ops. ORDER OPR. */ \
    OP_ISLT,
    OP_ISGE,
    OP_ISLE,
    OP_ISGT,

    OP_ISEQV,
    OP_ISNEV,

    OP_ISEQS,
    OP_ISNES,

    OP_ISEQN,
    OP_ISNEN,

    OP_ISEQP,
    OP_ISNEP,

    /* Unary test and copy ops. */
    OP_ISTC,
    OP_ISFC,

    OP_IST,
    OP_ISF,

    /* Unary ops. */
    OP_MOV,
    OP_NOT,
    OP_UNM,
    OP_LEN,

    /* Binary ops. ORDER OPR. VV last, POW must be next. */
    OP_ADDVN,
    OP_SUBVN,
    OP_MULVN,
    OP_DIVVN,
    OP_MODVN,

    OP_ADDNV,
    OP_SUBNV,
    OP_MULNV,
    OP_DIVNV,
    OP_MODNV,

    OP_ADDVV,
    OP_SUBVV,
    OP_MULVV,
    OP_DIVVV,
    OP_MODVV,

    OP_POW,
    OP_CAT,

    /* Constant ops. */
    OP_KSTR,
    OP_KCDATA,
    OP_KSHORT,
    OP_KNUM,
    OP_KPRI,

    OP_KNIL,

    /* Upvalue and function ops. */
    OP_UGET,

    OP_USETV,
    OP_USETS,
    OP_USETN,
    OP_USETP,

    OP_UCLO,

    OP_FNEW,

    /* Table ops. */
    OP_TNEW,

    OP_TDUP,

    OP_GGET,
    OP_GSET,

    OP_TGETV,
    OP_TGETS,
    OP_TGETB,

    OP_TSETV,
    OP_TSETS,
    OP_TSETB,

    OP_TSETM,

    /* Calls and vararg handling. T = tail call. */
    OP_CALLM,
    OP_CALL,
    OP_CALLMT,
    OP_CALLT,

    OP_ITERC,
    OP_ITERN,

    OP_VARG,

    OP_ISNEXT,

    /* Returns. */
    OP_RETM,
    OP_RET,
    OP_RET0,
    OP_RET1,

    /* Loops and branches. I/J = interp/JIT, I/C/L = init/call/loop. */
    OP_FORI,
    OP_JFORI,
    OP_FORL,
    OP_IFORL,
    OP_JFORL,

    OP_ITERL,
    OP_IITERL,
    OP_JITERL,

    OP_LOOP,
    OP_ILOOP,
    OP_JLOOP,

    OP_JMP,

    /* Function headers. I/J = interp/JIT, F/V/C = fixarg/vararg/C func. */
    OP_FUNCF,
    OP_IFUNCF,
    OP_JFUNCF,

    OP_FUNCV,
    OP_IFUNCV,
    OP_JFUNCV,

    OP_FUNCC,
    OP_FUNCCW
};

#endif
