
#include <stdio.h>
#include <fstream>
#include <cstring>
#include <iostream>
#include <vector>
#include <unordered_map>
#include <deque>

#include "opcode.h"

union AtomicInstruction
{
    uint32_t _raw;
    struct {
        LuaJITOpcode opcode;
        uint8_t A;
        uint8_t C;
        uint8_t B;
    } ACB;

    struct {
        LuaJITOpcode opcode;
        uint8_t A;
        uint16_t D;
    } AD;
};

enum LJFlags
{
    LJF_NONE       = 0x0,
    LJF_BIG_ENDIAN = 0x1,
    LJF_STRIPPED   = 0x2,
    LJF_HAS_FFI    = 0x4,
    LJF_FR2        = 0x8
};

enum LJBlockFlags
{
    LJBF_NONE       = 0x00,
    LJBF_HAS_CHILD  = 0x01,
    LJBF_VARIADIC   = 0x02,
    LJBF_HAS_FFI    = 0x04,
    LJB_JIT_DISABLE = 0x08,
    LJB_HAS_ILOOP   = 0x10
};

enum LJConstType
{
    LJC_Nil,
    LJC_Number,
    LJC_Integer,
    LJC_Boolean,
    LJC_String,
    LJC_Block,
    LJC_Table,
    LJC_Complex
};

struct Table;

struct Constant
{
    double numval;
    int64_t intval;
    std::string strval;
    struct {
        double im, re;
    } complxval;
    bool boolval;
    LJConstType type;
    Table* tabval;

    void ToString(char* buff);
};

struct Table
{
    std::unordered_map<Constant*, Constant*> _dictionary;
    std::vector<Constant*> _array;
};

void Constant::ToString(char *buff)
{
    buff[0] = 0;

    switch (type)
    {
        case LJC_Nil:
            sprintf(buff, "nil");
            break;
        case LJC_Number:
            sprintf(buff, "[Number] %lf", numval);
            break;
        case LJC_Integer:
            sprintf(buff, "[Integer] %ld", intval);
            break;
        case LJC_Boolean:
            if (boolval)
                sprintf(buff, "true");
            else
                sprintf(buff, "false");

            break;
        case LJC_String:
            sprintf(buff, "[String] \"%s\"", strval.c_str());
            break;
        case LJC_Block:
            sprintf(buff, "[FunctionBlock%ld]", intval);
            break;
        case LJC_Table:
            if (!tabval->_array.empty())
            {
                strcat(buff, "array {");

                for (Constant* c : tabval->_array)
                {
                    char buff[256];
                    c->ToString(buff);
                    sprintf(buff + strlen(buff), "      %s,\n", buff);
                }

                strcat(buff, "    }");
            }

            if (!tabval->_dictionary.empty())
            {
                strcat(buff, "dictionary {");

                for (std::pair<Constant*, Constant*> p : tabval->_dictionary)
                {
                    char buff1[256];
                    char buff2[256];
                    p.first->ToString(buff1);
                    p.second->ToString(buff2);
                    sprintf(buff + strlen(buff), "      %s: %s,\n", buff1, buff2);
                }

                strcat(buff, "    }");
            }
            break;
        case LJC_Complex:
            sprintf(buff, "[Complex] {im=%lf, re=%lf}", complxval.im, complxval.re);
            break;
    }
}

const char* inst_parm_map[] = {
    "v?v", "v?v", "v?v", "v?v", "v?v", "v?v", "v?s", "v?s", "v?n", "v?n", "v?p", "v?p",
    "d?v", "d?v", "??v", "??v", "d?v", "d?v", "d?v", "d?v", "dvn", "dvn", "dvn", "dvn",
    "dvn", "dvn", "dvn", "dvn", "dvn", "dvn", "dvv", "dvv", "dvv", "dvv", "dvv", "dvv",
    "drr", "d?s", "d?c", "d?l", "d?n", "d?p", "b?b", "d?u", "u?v", "u?s", "u?n", "u?p",
    "r?j", "d?f", "d?i", "d?t", "d?s", "v?s", "dvv", "dvs", "dvi", "vvv", "vvs", "vvi",
    "b?n", "bii", "bii", "b?i", "b?i", "bii", "bii", "bii", "b?j", "b?i", "r?i", "r?i",
    "r?i", "b?j", "b?j", "b?j", "b?j", "b?i", "b?j", "b?j", "b?i", "r?j", "r?j", "r?i",
    "r?j", "r??", "r??", "r?i", "r??", "r??", "r?i", "r??", "r??"
};

template<typename T>
void rt(std::ifstream& s, T* out) {
    s.read(reinterpret_cast<char*>(out), sizeof(T));
}

void rs(std::ifstream& s, const uint64_t& len, std::string& str)
{
    char tmp;
    for (uint64_t i = 0; i < len; i++)
    {
        rt(s, &tmp);
        str += tmp;
    }
}

uint64_t read_varint(std::ifstream& s, bool* firstbit = nullptr)
{
    uint8_t byte;
    rt(s, &byte);

    if (firstbit)
    {
        *firstbit = byte & 1;
        byte >>= 1;
    }

    uint64_t value = byte;
    uint32_t bitshift = 0;

    if (value >= 0x80)
    {
        value &= 0x7f;

        do
        {
            rt(s, &byte);
            bitshift += 7;
            value |= (byte & 0x7f) << bitshift;
        } while (byte >= 0x80);
    }

    return value;
}

double read_vardouble(std::ifstream& s)
{
    uint64_t lo = read_varint(s) & UINT32_MAX;
    uint64_t hi = read_varint(s) & UINT32_MAX;

    uint64_t raw = hi << 32 | lo;
    return *reinterpret_cast<double*>(&raw);
}

struct LJHeader
{
    uint8_t esc; // always 0x1b
    uint16_t magic; // 'LJ' 0x4A4C
    uint8_t version;
    LJFlags flags;
    std::string name;

    void ReadFrom(std::ifstream& ifs)
    {
        rt(ifs, &esc);
        rt(ifs, &magic);
        rt(ifs, &version);
        flags = static_cast<LJFlags>(read_varint(ifs));

        if (!(flags & LJF_STRIPPED))
        {
            uint64_t len = read_varint(ifs);

            rs(ifs, len, name);
        }
        else name = "<STRIPPED>";
    }

    void Print()
    {
        puts("    ------------------------------");
        printf("    Version: %02x\n", version);
        printf("    Flags:");

        if (flags)
        {
            if (flags & LJF_BIG_ENDIAN) printf(" LJF_BIG_ENDIAN");
            if (flags & LJF_STRIPPED)   printf(" LJF_STRIPPED");
            if (flags & LJF_HAS_FFI)    printf(" LJF_HAS_FFI");
            if (flags & LJF_FR2)        printf(" LJF_FR2");
            puts("");
        }
        else puts(" NONE");

        printf("    Name: %s\n", name.c_str());
        puts("    ------------------------------");
    }
};

struct LJBlockInfo
{
    LJBlockFlags flags;
    uint8_t argument_count;
    uint8_t framesize;
    uint8_t upvalue_count;
    uint64_t complex_constant_count;
    uint64_t numeric_constant_count;
    uint64_t instruction_count;
    uint64_t debug_info_size;
    uint64_t first_line_number;
    uint64_t lines_count;

    void ReadFrom(std::ifstream& ifs, bool isStripped)
    {
        flags = static_cast<LJBlockFlags>(read_varint(ifs));

        rt(ifs, &argument_count);
        rt(ifs, &framesize);
        rt(ifs, &upvalue_count);

        complex_constant_count = read_varint(ifs);
        numeric_constant_count = read_varint(ifs);
        instruction_count = read_varint(ifs);

        if (isStripped)
        {
            debug_info_size = 0;
        }
        else
        {
            debug_info_size = read_varint(ifs);

            if (debug_info_size > 0)
            {
                instruction_count = read_varint(ifs);
                lines_count = read_varint(ifs);
            }
        }
    }

    void Print()
    {
        printf("    Flags:");

        if (flags)
        {
            if (flags & LJBF_HAS_CHILD)     printf(" LJBF_HAS_CHILD");
            if (flags & LJBF_VARIADIC)      printf(" LJBF_VARIADIC");
            if (flags & LJBF_HAS_FFI)       printf(" LJBF_HAS_FFI");
            if (flags & LJB_JIT_DISABLE)    printf(" LJB_JIT_DISABLE");
            if (flags & LJB_HAS_ILOOP)      printf(" LJB_HAS_ILOOP");
            puts("");
        }
        else puts(" NONE");

        printf("    argument_count = %d\n", argument_count);
        printf("    framesize = %d\n", framesize);
        printf("    upvalue_count = %d\n", upvalue_count);
        printf("    complex_constant_count = %ld\n", complex_constant_count);
        printf("    numeric_constant_count = %ld\n", numeric_constant_count);
        printf("    instruction_count = %ld\n", instruction_count);
        printf("    debug_info_size = %ld\n", debug_info_size);
        printf("    first_line_number = %ld\n", first_line_number);
        printf("    lines_count = %ld\n", lines_count);
        puts("    ------------------------------");
    }
};

Constant* read_table_constant(std::ifstream& s)
{
    uint64_t type = read_varint(s);

    if (type >= 5) // string
    {
        Constant* c = new Constant;
        rs(s, type - 5, c->strval);
        c->type = LJC_String;
        return c;
    }

    if (type == 3)
    {
        Constant* c = new Constant;
        c->type = LJC_Integer;
        c->intval = static_cast<int64_t>(read_varint(s));
        return c;
    }

    if (type == 4)
    {
        Constant* c = new Constant;
        c->type = LJC_Number;
        c->numval = read_vardouble(s);
        return c;
    }

    if (type == 2)
    {
        Constant* c = new Constant;
        c->type = LJC_Boolean;
        c->boolval = true;
        return c;
    }

    if (type == 1)
    {
        Constant* c = new Constant;
        c->type = LJC_Boolean;
        c->boolval = false;
        return c;
    }

    if (type == 0)
    {
        Constant* c = new Constant;
        c->type = LJC_Nil;
        return c;
    }

    return nullptr;
}

Table* read_table(std::ifstream& s)
{
    uint64_t array_item_count = read_varint(s);
    uint64_t hash_item_count  = read_varint(s);

    Table* res = new Table();

    for (uint64_t n = 0; n < array_item_count; n++)
        res->_array.push_back(read_table_constant(s));

    for (uint64_t n = 0; n < hash_item_count; n++)
    {
        Constant* key = read_table_constant(s);
        Constant* value = read_table_constant(s);

        res->_dictionary.insert(std::pair<Constant*, Constant*>(key, value));
    }

    return res;
}

const char* opcode2str(const LuaJITOpcode& opc)
{
    switch (opc)
    {
        case LuaJITOpcode::OP_ISLT: return "ISLT";
        case LuaJITOpcode::OP_ISGE: return "ISGE";
        case LuaJITOpcode::OP_ISLE: return "ISLE";
        case LuaJITOpcode::OP_ISGT: return "ISGT";
        case LuaJITOpcode::OP_ISEQV: return "ISEQV";
        case LuaJITOpcode::OP_ISNEV: return "ISNEV";
        case LuaJITOpcode::OP_ISEQS: return "ISEQS";
        case LuaJITOpcode::OP_ISNES: return "ISNES";
        case LuaJITOpcode::OP_ISEQN: return "ISEQN";
        case LuaJITOpcode::OP_ISNEN: return "ISNEN";
        case LuaJITOpcode::OP_ISEQP: return "ISEQP";
        case LuaJITOpcode::OP_ISNEP: return "ISNEP";
        case LuaJITOpcode::OP_ISTC: return "ISTC";
        case LuaJITOpcode::OP_ISFC: return "ISFC";
        case LuaJITOpcode::OP_IST: return "IST";
        case LuaJITOpcode::OP_ISF: return "ISF";
        case LuaJITOpcode::OP_MOV: return "MOV";
        case LuaJITOpcode::OP_NOT: return "NOT";
        case LuaJITOpcode::OP_UNM: return "UNM";
        case LuaJITOpcode::OP_LEN: return "LEN";
        case LuaJITOpcode::OP_ADDVN: return "ADDVN";
        case LuaJITOpcode::OP_SUBVN: return "SUBVN";
        case LuaJITOpcode::OP_MULVN: return "MULVN";
        case LuaJITOpcode::OP_DIVVN: return "DIVVN";
        case LuaJITOpcode::OP_MODVN: return "MODVN";
        case LuaJITOpcode::OP_ADDNV: return "ADDNV";
        case LuaJITOpcode::OP_SUBNV: return "SUBNV";
        case LuaJITOpcode::OP_MULNV: return "MULNV";
        case LuaJITOpcode::OP_DIVNV: return "DIVNV";
        case LuaJITOpcode::OP_MODNV: return "MODNV";
        case LuaJITOpcode::OP_ADDVV: return "ADDVV";
        case LuaJITOpcode::OP_SUBVV: return "SUBVV";
        case LuaJITOpcode::OP_MULVV: return "MULVV";
        case LuaJITOpcode::OP_DIVVV: return "DIVVV";
        case LuaJITOpcode::OP_MODVV: return "MODVV";
        case LuaJITOpcode::OP_POW: return "POW";
        case LuaJITOpcode::OP_CAT: return "CAT";
        case LuaJITOpcode::OP_KSTR: return "KSTR";
        case LuaJITOpcode::OP_KCDATA: return "KCDATA";
        case LuaJITOpcode::OP_KSHORT: return "KSHORT";
        case LuaJITOpcode::OP_KNUM: return "KNUM";
        case LuaJITOpcode::OP_KPRI: return "KPRI";
        case LuaJITOpcode::OP_KNIL: return "KNIL";
        case LuaJITOpcode::OP_UGET: return "UGET";
        case LuaJITOpcode::OP_USETV: return "USETV";
        case LuaJITOpcode::OP_USETS: return "USETS";
        case LuaJITOpcode::OP_USETN: return "USETN";
        case LuaJITOpcode::OP_USETP: return "USETP";
        case LuaJITOpcode::OP_UCLO: return "UCLO";
        case LuaJITOpcode::OP_FNEW: return "FNEW";
        case LuaJITOpcode::OP_TNEW: return "TNEW";
        case LuaJITOpcode::OP_TDUP: return "TDUP";
        case LuaJITOpcode::OP_GGET: return "GGET";
        case LuaJITOpcode::OP_GSET: return "GSET";
        case LuaJITOpcode::OP_TGETV: return "TGETV";
        case LuaJITOpcode::OP_TGETS: return "TGETS";
        case LuaJITOpcode::OP_TGETB: return "TGETB";
        case LuaJITOpcode::OP_TSETV: return "TSETV";
        case LuaJITOpcode::OP_TSETS: return "TSETS";
        case LuaJITOpcode::OP_TSETB: return "TSETB";
        case LuaJITOpcode::OP_TSETM: return "TSETM";
        case LuaJITOpcode::OP_CALLM: return "CALLM";
        case LuaJITOpcode::OP_CALL: return "CALL";
        case LuaJITOpcode::OP_CALLMT: return "CALLMT";
        case LuaJITOpcode::OP_CALLT: return "CALLT";
        case LuaJITOpcode::OP_ITERC: return "ITERC";
        case LuaJITOpcode::OP_ITERN: return "ITERN";
        case LuaJITOpcode::OP_VARG: return "VARG";
        case LuaJITOpcode::OP_ISNEXT: return "ISNEXT";
        case LuaJITOpcode::OP_RETM: return "RETM";
        case LuaJITOpcode::OP_RET: return "RET";
        case LuaJITOpcode::OP_RET0: return "RET0";
        case LuaJITOpcode::OP_RET1: return "RET1";
        case LuaJITOpcode::OP_FORI: return "FORI";
        case LuaJITOpcode::OP_JFORI: return "JFORI";
        case LuaJITOpcode::OP_FORL: return "FORL";
        case LuaJITOpcode::OP_IFORL: return "IFORL";
        case LuaJITOpcode::OP_JFORL: return "JFORL";
        case LuaJITOpcode::OP_ITERL: return "ITERL";
        case LuaJITOpcode::OP_IITERL: return "IITERL";
        case LuaJITOpcode::OP_JITERL: return "JITERL";
        case LuaJITOpcode::OP_LOOP: return "LOOP";
        case LuaJITOpcode::OP_ILOOP: return "ILOOP";
        case LuaJITOpcode::OP_JLOOP: return "JLOOP";
        case LuaJITOpcode::OP_JMP: return "JMP";
        case LuaJITOpcode::OP_FUNCF: return "FUNCF";
        case LuaJITOpcode::OP_IFUNCF: return "IFUNCF";
        case LuaJITOpcode::OP_JFUNCF: return "JFUNCF";
        case LuaJITOpcode::OP_FUNCV: return "FUNCV";
        case LuaJITOpcode::OP_IFUNCV: return "IFUNCV";
        case LuaJITOpcode::OP_JFUNCV: return "JFUNCV";
        case LuaJITOpcode::OP_FUNCC: return "FUNCC";
        case LuaJITOpcode::OP_FUNCCW: return "FUNCCW";
        default: return "UNKWNON";
    }
}

const char* decode_parm_type_name(const char& c)
{
    switch (c)
    {
        case 'v': return "var";
        case 's': return "str";
        case 'n': return "num";
        case 'p': return "pri";
        case 'd': return "dst";
        case 'r': return "rbase";
        case 'c': return "cdata";
        case 'l': return "lits";
        case 'b': return "base";
        case 'j': return "jump";
        case 'i': return "lit";
        case 'u': return "uv";
        case 't': return "tab";
        case '?': return "_";
        default: return "<unk>";
    }
}

bool stringify_oparand(const uint16_t& val, const char& parm, char* buf, const uint64_t& ncc,
    const std::vector<Constant*>& consts)
{
    switch (parm)
    {
        case 'v':
            sprintf(buf, "VAR%d", val);
            return true;
        case 's':
            sprintf(buf, "_STR[%d] (\"%s\")", val, consts[consts.size() - val - 1]->strval.c_str());
            return true;
        case 'n':
            {
                char buff[64];
                consts[ncc - val]->ToString(buff);
                sprintf(buf, "_NUM[%d] (%s)", val, buff);
            }
            return true;
        case 'p':
            if (val == 0)
                sprintf(buf, "nil");
            else if (val == 1)
                sprintf(buf, "false");
            else if (val == 2)
                sprintf(buf, "true");
            else
                sprintf(buf, "<unknown_primitive_%02xh>", val);

            return true;
        case 'd':
            sprintf(buf, ">VAR%d", val);
            return true;
        case 'r':
            sprintf(buf, "RBASE%d", val);
            return true;
        case 'c':
            sprintf(buf, "_CONST[%d]", val);
            return true;
        case 'l':
            sprintf(buf, "%d", static_cast<int16_t>(val));
            return true;
        case 'b':
            sprintf(buf, "BASE%d", val);
            return true;
        case 'j':
            {
                int64_t offs = (int64_t)val - 0x8000;
                if (offs > 0)
                    sprintf(buf, "$+%02xh", (unsigned)offs);
                else
                    sprintf(buf, "$-%02xh", (unsigned)-offs);
            }
            return true;
        case 'i':
            sprintf(buf, "%u", val);
            return true;
        case 'u':
            sprintf(buf, "UPV%d", val);
            return true;
        case 't':
            sprintf(buf, "_TAB[%d]", val);
            return true;
        case 'f':
            sprintf(buf, "_FUNC[%d] (BLOCK %ld)", val, consts[consts.size() - val - 1]->intval);
            return true;
        default:
            return false;
    }
}

inline void print_inst(uint64_t n, AtomicInstruction& i, const uint64_t& ncc,
    const std::vector<Constant*>& consts)
{
    const char* parm = inst_parm_map[i.ACB.opcode];
    bool isAD = parm[1] == '?' && parm[2] != '?';

    char buff[65536] = {0};
    char tmp[8192];
    bool append_comma = false;

    if (isAD)
    {
        if (stringify_oparand(i.AD.A, parm[0], tmp, ncc, consts))
        {
            append_comma = true;
            strcat(buff, tmp);
        }

        if (stringify_oparand(i.AD.D, parm[2], tmp, ncc, consts))
        {
            if (append_comma) strcat(buff, ", ");
            strcat(buff, tmp);
        }
    }
    else
    {
        if (stringify_oparand(i.ACB.A, parm[0], tmp, ncc, consts))
        {
            append_comma = true;
            strcat(buff, tmp);
        }

        if (stringify_oparand(i.ACB.B, parm[1], tmp, ncc, consts))
        {
            if (append_comma) strcat(buff, ", ");
            strcat(buff, tmp);
        }

        if (stringify_oparand(i.ACB.C, parm[2], tmp, ncc, consts))
        {
            if (append_comma) strcat(buff, ", ");
            strcat(buff, tmp);
        }
    }

    printf("    [0x%04lx] <%02x %02x %02x %02x> %-6s %s\n",
        n,
        i.ACB.opcode,
        i.ACB.A,
        i.ACB.C,
        i.ACB.B,
        opcode2str(i.ACB.opcode),
        buff);
}

int main(int argc, char const *argv[])
{
    std::ifstream ifs("../bin.bin", std::ios::in | std::ios::binary | std::ios::ate);
    ifs.seekg(0, std::ios::end);
    size_t file_size = ifs.tellg();
    printf("Size of file: %lu\n", file_size);
    ifs.seekg(0, std::ios::beg);

    LJHeader header;
    header.ReadFrom(ifs);

    uint32_t block_n = 1;
    if (header.magic == 0x4A4C && header.esc == 0x1B)
    {
        puts("Magic OK!");

        header.Print();

        std::deque<uint64_t> blocks;

        while (!ifs.eof())
        {
            printf("\n\n--------------------------------- BLOCK %d ---------------------------------\n{\n", block_n);
            uint64_t size = read_varint(ifs);
            size_t start_pos = ifs.tellg();

            LJBlockInfo info;
            info.ReadFrom(ifs, header.flags & LJF_STRIPPED);
            info.Print();

            // Read instructions
            std::vector<AtomicInstruction> insts;

            for (uint64_t n = 0; n < info.instruction_count; n++)
            {
                AtomicInstruction i;
                rt(ifs, &i);
                insts.push_back(i);
            }

            size_t pos = ifs.tellg();
            printf("    Read %lu instructions, pos: %lx\n", insts.size(), pos);

            // Read upvalue references
            std::vector<uint16_t> upval_refs;

            for (uint64_t n = 0; n < info.upvalue_count; n++)
            {
                uint16_t val;
                rt(ifs, &val);
                upval_refs.push_back(val);
                printf("    [%-5ld] Upvalue: %04x\n", n, val);
            }

            pos = ifs.tellg();
            printf("    Read %lu upvalue references, pos: %lx\n", upval_refs.size(), pos);

            std::vector<Constant*> consts;

            // Read constants
            for (uint64_t n = 0; n < info.complex_constant_count; n++)
            {
                uint64_t type = read_varint(ifs);

                if (type >= 5) // string
                {
                    uint64_t len = type - 5;
                    Constant* c = new Constant;
                    c->type = LJC_String;
                    rs(ifs, len, c->strval);
                    printf("    [%-5ld] String constant: (type=%lx,len=%ld) '%s'\n", n, type, len, c->strval.c_str());
                    consts.push_back(c);
                }
                else if (type == 1) // table
                {
                    Constant* c = new Constant;
                    c->type = LJC_Table;
                    Table* t = read_table(ifs);
                    c->tabval = t;
                    consts.push_back(c);
                    printf("    [%-5ld] Read a table with %ld array and %ld hash entries\n", n, t->_array.size(), t->_dictionary.size());

                    if (!t->_array.empty())
                    {
                        puts("    array {");

                        for (Constant* c : t->_array)
                        {
                            char buff[256];
                            c->ToString(buff);
                            printf("      %s,\n", buff);
                        }

                        puts("    }");
                    }

                    if (!t->_dictionary.empty())
                    {
                        puts("    dictionary {");

                        for (std::pair<Constant*, Constant*> p : t->_dictionary)
                        {
                            char buff1[256];
                            char buff2[256];
                            p.first->ToString(buff1);
                            p.second->ToString(buff2);
                            printf("      %s: %s,\n", buff1, buff2);
                        }

                        puts("    }");
                    }
                }
                else if (type == 4) // complex number
                {
                    Constant* c = new Constant;
                    c->type = LJC_Complex;
                    c->complxval.re = read_vardouble(ifs);
                    c->complxval.im = read_vardouble(ifs);
                    consts.push_back(c);
                    printf("    [%-5ld] Read complex number: re=%.2lf im=%.2lf\n", n, c->complxval.re, c->complxval.im);
                }
                else if (type == 2 || type == 3)
                {
                    Constant* c = new Constant;
                    c->type = LJC_Number;
                    c->numval = read_vardouble(ifs);
                    consts.push_back(c);
                    printf("    [%-5ld] Read number: %lf\n", n, c->numval);
                }
                else if (type == 0)
                {
                    if (blocks.empty())
                        printf("    [%-5ld] Block queue is empty, could not resolve function reference", n);
                    else
                    {
                        uint64_t id = blocks.front();
                        blocks.pop_front();
                        Constant* c = new Constant;
                        c->type = LJC_Block;
                        c->intval = id;
                        consts.push_back(c);
                        printf("    [%-5ld] Block reference: %ld\n", n, id);
                    }
                }
                else
                    printf("    [%-5ld] Unknown const type: 0x%lx\n", n, type);
            }

            // Read number constants
            for (uint64_t n = 0; n < info.numeric_constant_count; n++)
            {
                bool firstbit;
                uint64_t lo = read_varint(ifs, &firstbit);

                if (firstbit)
                {
                    uint64_t hi = read_varint(ifs);

                    uint64_t rawnum = (hi << 32) | lo;

                    Constant* c = new Constant;
                    c->type = LJC_Number;
                    c->numval = *reinterpret_cast<double*>(&rawnum);
                    consts.push_back(c);

                    printf("    [%-5ld] Number constant: %lf\n", n, c->numval);
                }
                else
                {
                    Constant* c = new Constant;
                    c->type = LJC_Integer;
                    c->intval = static_cast<int64_t>(lo);
                    consts.push_back(c);

                    printf("    [%-5ld] Integer constant: %ld\n", n, c->intval);
                }
            }

            uint64_t read_size;
            read_size = (uint64_t)ifs.tellg() - start_pos;
            printf("    Block size: %lx, actually read: %lx\n", size, read_size);

            for (uint64_t n = 0; n < info.instruction_count; n++)
                print_inst(n, insts[n], info.complex_constant_count, consts);

            blocks.push_front(block_n);
            block_n++;

            puts("}");

            if (read_size < size)
                goto __exit;
        }
    }
    else goto __exit;

    __exit:
    ifs.close();
    return 0;
}
